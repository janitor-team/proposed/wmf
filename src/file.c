/*************************************************************
 * WebMailFolder                                             *
 * COPYRIGHT 1998 by Norbert Kuemin <norbert.kuemin@gmx.net> *
 *************************************************************/

#define FILE_FILE
#include "wmf.h"


int FileExist (char *FileName) {
    FILE *fd;
    
    if ((config.DebugLevel != WMF_NOVALUE) && (config.DebugLevel & 4))
       printf(".FileExist(%s)\n",FileName);
    fd = fopen(FileName,"r");
    if (fd == NULL) {
        return FALSE;
    } else {
        fclose(fd);
        return TRUE;
    }
}

char *CheckHomeDir(char *Name) {
    int newsize;
    char *newname;
    
    if ((config.DebugLevel != WMF_NOVALUE) && (config.DebugLevel & 4))
       printf(".CheckHomeDir(%s)\n",Name);
    if (Name != NULL) {
        if (Name[0] == '~') {
            newsize = strlen(Name)+strlen((char *)getenv("HOME")+1);
            newname = newchar(newsize);
            strcpy(newname,(char *)getenv("HOME"));
            Name++;
            strcat(newname,Name);
            Name--;
	    if ((config.DebugLevel != WMF_NOVALUE) && (config.DebugLevel & 2))
	        printf("..%s -> %s\n",Name,newname);
	} else {
	    newname = dupchar(Name);
	}
	return newname;
    }
    return Name;
}


void NewFile(char *FileName, char *Text) {
    FILE *fd;
    
    if ((config.DebugLevel != WMF_NOVALUE) && (config.DebugLevel & 4))
	if (config.NewFilePerm != 0) {
	    printf(".NewFile(%s) Perm=%o\n",FileName,(int) config.NewFilePerm);
	} else {
	    printf(".NewFile(%s)\n",FileName);
	}
    fd = fopen(FileName,"w");
    if (fd != NULL) {
        fprintf(fd,"%s",Text);
        fclose(fd);
    }
    if (config.NewFilePerm != 0) {
	if ((chmod(FileName, config.NewFilePerm)<0) && (config.DebugLevel & 4)) {
	    printf(".NewFile(%s) couldn't set perm to %o\n",FileName,(int) config.NewFilePerm);
	}
    }
}

void AppendFile(char *FileName, char *Text) {
    static FILE *fd1 = NULL;
    static char *LastFileName = NULL;
    
    if ((config.DebugLevel != WMF_NOVALUE) && (config.DebugLevel & 4))
       printf(".AppendFile(%s,\"%s\")\n",FileName,Text);
    if ((LastFileName == NULL) || (strcmp(LastFileName,FileName) != 0)) {
	if (fd1 != NULL) {
	    fclose(fd1);
	    fd1 = NULL;
	}
	if (LastFileName != NULL) {
	    freechar(LastFileName);
	    LastFileName = NULL;
	}
	if (strlen(FileName) > 0) {
	    LastFileName = dupchar(FileName);
	    fd1 = fopen(FileName,"a");
	}
    }
    if (fd1 != NULL) {
        fprintf(fd1,"%s",Text);
    }
}


void IncludeFile(char *FileName, char *FileInclude) {

    FILE *fd;
    char line[BUFSIZ];

    if (FileExist(FileInclude) == TRUE) {
	fd = fopen(FileInclude,"r");
	if (fd != NULL) {
	    while (feof(fd) == 0) {
		fgets(line,BUFSIZ,fd);
		if (feof(fd) == 0)
		    AppendFile(FileName,line);
	    }
	    fclose(fd);
	}
    }
}

char *GetFileName(int number) {
    int dirlen;
    char *file;
    
    if ((config.DebugLevel != WMF_NOVALUE) && (config.DebugLevel & 4))
       printf(".GetFileName(%d)\n",number);
    /* calculate filename = dir + / + number.html + \0 */
    if (config.Directory == NULL) {
	dirlen = 0;
    } else {
	dirlen = strlen(config.Directory);
    }
    if (dirlen == 0) {
	file = newchar(254);
	sprintf(file,"%d%s",number,config.Extension);
    } else {
	file = newchar(dirlen + 255);
	sprintf(file,"%s/%d%s",config.Directory,number,config.Extension);
    }
    return file;
}

int GetNextFileNumber(int start) {
    int counter;
    char *file;
    
    if ((config.DebugLevel != WMF_NOVALUE) && (config.DebugLevel & 4))
       printf(".GetNextFileNumber\n");
    counter = start;
    while ((counter >= 0) && (counter < 1000000)) {
	file = GetFileName(counter);
	if (!FileExist(file)) {
	    freechar(file);
	    return counter;
	} else {
	    freechar(file);
	    counter++;
	}
    }
    return counter;
}


void CleanDirectory(char *DirName) {
    DIR *dd;
    struct dirent *DirEntry;
    char *FileName;
        
    if ((config.DebugLevel != WMF_NOVALUE) && (config.DebugLevel & 4))
       printf(".CleanDirectory(%s)\n",DirName);
    dd = opendir(DirName);
    DirEntry = readdir(dd);
    while (DirEntry != NULL) {
	if ((strcmp(DirEntry->d_name,".") != 0) && 
	    (strcmp(DirEntry->d_name,"..") != 0)) {
	    FileName = newchar(strlen(DirName)+strlen(DirEntry->d_name)+2);
            sprintf(FileName,"%s/%s",DirName,DirEntry->d_name);
	    remove(FileName);
	    freechar(FileName);
	}
        DirEntry = readdir(dd);
    }
    closedir(dd);
}

void DelIndexFiles(void) {
    DIR *dd;
    struct dirent *DirEntry;
    char *FileName;
        
    if ((config.DebugLevel != WMF_NOVALUE) && (config.DebugLevel & 4))
       printf(".DelIndexFiles\n");
    dd = opendir(config.Directory);
    DirEntry = readdir(dd);
    while (DirEntry != NULL) {
	if ((strncmp(DirEntry->d_name,"date",4) == 0) || 
	    (strncmp(DirEntry->d_name,"subject",7) == 0) ||
	    (strncmp(DirEntry->d_name,"thread",6) == 0) ||
	    (strncmp(DirEntry->d_name,"stats",6) == 0) ||
	    (strncmp(DirEntry->d_name,"author",6) == 0)) {
	    FileName = newchar(strlen(config.Directory)
	        + strlen(DirEntry->d_name) + 2);
            sprintf(FileName,"%s/%s",config.Directory,DirEntry->d_name);
	    if (config.DebugLevel & 2)
		printf(".del %s\n",FileName);
	    remove(FileName);
	    freechar(FileName);
	}
        DirEntry = readdir(dd);
    }
    closedir(dd);
}
