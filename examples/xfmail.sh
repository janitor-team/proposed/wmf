#/bin/sh
#
# Converting a folder (xfmail) to html
SRC_DIR="./mail"
DEST_DIR="./html"
LIST_NAME="Test archive"
LIST_DEBUG=0

DIR="`ls ${SRC_DIR}/*`"
for filename in ${DIR} ; do
  echo "${filename}"
  wmf -w 3 -m ${filename} -d ${DEST_DIR} -n "${LIST_NAME}" -x ${LIST_DEBUG}
done
echo "Indexing ..."
wmf -w 0 -d ${DEST_DIR} -n ${LIST_NAME} -x ${LIST_DEBUG}
